plugins {
    id 'com.android.application'
    id 'org.jetbrains.kotlin.android'
    id 'com.mikepenz.aboutlibraries.plugin'
}

android {
    compileSdk 34
    namespace 'de.nulide.findmydevice'
    packagingOptions {
        resources {
            excludes += ['META-INF/LICENSE.md', 'META-INF/NOTICE.md']
        }
    }

    lint {
        baseline = file("lint-baseline.xml")
        lintOptions {
            disable 'MissingTranslation'
        }
    }

    defaultConfig {
        applicationId "de.nulide.findmydevice"
        minSdkVersion 24
        targetSdkVersion 34
        versionCode 25
        versionName "0.5.0"
        multiDexEnabled true

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }

    flavorDimensions "default"
    productFlavors {
        prod {
            dimension "default"
        }
        dev {
            dimension "default"
            applicationIdSuffix ".dev"
        }
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_17
        targetCompatibility JavaVersion.VERSION_17
    }
    kotlin {
        jvmToolchain(17)
    }

    buildFeatures {
        viewBinding true
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])

    // AndroidX
    implementation 'androidx.activity:activity-ktx:1.9.0'
    implementation 'androidx.annotation:annotation:1.8.0'
    implementation 'androidx.appcompat:appcompat:1.7.0'
    implementation 'androidx.core:core-ktx:1.13.1'
    implementation 'androidx.concurrent:concurrent-futures-ktx:1.1.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.1.4'
    implementation "androidx.fragment:fragment-ktx:1.7.1"
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    implementation 'androidx.lifecycle:lifecycle-viewmodel-ktx:2.8.1'
    implementation 'androidx.preference:preference-ktx:1.2.1'

    // CameraX
    def camerax_version = "1.3.3"
    implementation "androidx.camera:camera-core:${camerax_version}"
    implementation "androidx.camera:camera-camera2:${camerax_version}"
    implementation "androidx.camera:camera-lifecycle:${camerax_version}"
    implementation "androidx.camera:camera-video:${camerax_version}"
    implementation "androidx.camera:camera-view:${camerax_version}"
    implementation "androidx.camera:camera-extensions:${camerax_version}"

    // Testing
    testImplementation 'junit:junit:4.13.2'
    androidTestImplementation 'androidx.test.ext:junit:1.1.5'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.5.1'

    implementation 'com.android.volley:volley:1.2.1'
    implementation 'com.android.support:multidex:1.0.3'
    implementation 'com.google.android.material:material:1.12.0'

    implementation 'com.fasterxml.jackson.core:jackson-databind:2.12.7.1'
    implementation 'com.github.iamrobj:NotificationHelperLibrary:2.0.5'
    implementation 'com.github.Stjinchan:ExpandableCardView:1.3.0-beta02'
    implementation 'com.github.UnifiedPush:android-connector:2.1.1'
    implementation "org.apache.maven:maven-artifact:3.9.4" // for semantic versions
    implementation 'org.bouncycastle:bcprov-jdk18on:1.76'

    // https://github.com/mikepenz/AboutLibraries/issues/881
    //implementation "com.mikepenz:aboutlibraries:10.8.3"
    implementation "com.mikepenz:aboutlibraries-core-android:10.8.3"
    implementation("com.mikepenz:aboutlibraries:10.8.3") {
        exclude group: 'com.mikepenz', module: 'aboutlibraries-core'
    }

    // XXX: only used by javax.xml.bind.DatatypeConverter
    implementation 'jakarta.xml.bind:jakarta.xml.bind-api:2.3.2'
    implementation 'xerces:xercesImpl:2.12.1'

    // Shizuku
    implementation "dev.rikka.shizuku:api:12.1.0"
    implementation "dev.rikka.shizuku:provider:12.1.0"

    // https://stackoverflow.com/questions/75263047/duplicate-class-in-kotlin-android
    constraints {
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.8.0") {
            because("kotlin-stdlib-jdk7 is now a part of kotlin-stdlib")
        }
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.8.0") {
            because("kotlin-stdlib-jdk8 is now a part of kotlin-stdlib")
        }
    }
}
